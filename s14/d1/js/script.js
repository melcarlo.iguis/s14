// sample of single line comments

/*sample of multiple 
  line comments
*/

console.log	('I am External');

let num = 10;

console.log(num);


myVariable = 'New Initialize value';

console.log(myVariable);

// Declaring Multiple Variables

let firstName = 'Mel Carlo';
let lastName = 'Iguis';
console.log(firstName, lastName);

let word1 = 'is';
let word2 = 'bootcamper';
let word3 = 'of';
let word4 = 'School';
let word5 = 'Zuitt Coding';
let word6 = 'a';
let word7 = 'Institute';
let word8 = 'Bootcamp';
let space = ' ';

let sentence =  word5 + space + word8 + space + word7 + space + word1 +  space + word6 +space +word4 +space + word3 + space + word2;
console.log(sentence);

//Template literals (``)

//${} - placeholder
let sentenceLiteral = `${firstName} ${word1} ${word6} ${word2} ${word3} ${word5} ${word8} ${word7}` + ".";
console.log(sentenceLiteral);


//number data type

let numString1 = '7';
let numString2 = '5';
let numA = 7;
let numB = 5;
let num3 = 5.5;
let num4 = .5;

console.log(numString1+ numString2);
console.log(numA+numB);
console.log(numA+num3);
console.log(num3+num4);

// force coercion - when one data type is force to change to complete and operation.

console.log(numString1 +numA);
console.log(num3 +numString2);

//parseInt()

console.log(num4 + parseInt(numString1));

let sum = numA + parseInt(numString1);
console.log(sum);

// Mathematical Operations ()
// subtraction

console.log(numA-num3);
console.log(num3-num4);
console.log(numString1-numB);


let sample = ('Mel');
console.log(sample - numString1); 
// NaN - result when trying to perform mathematical operations between strings and number.  

// Multiplication

console.log(numB * numA);
console.log(numString1 * numA);
console.log(numString1 * numString2);

let product = numA * numB;
let product2 = numString1 * numA;
let product3 = numString1 * numString2;


// Division

console.log(product/numB);
console.log(product2/5);
console.log(numString2/numString1);

// modulo - remainder of division


console.log(product2 % numB);
console.log(product3%product2);


// Boolean - true or false

let isAdmin = true;
let isMarried = false;


// you can concatenate string and Boolean

console.log('Rommel is he' )



//Arrays
let array = ['Goku', 'Piccolo', 'Gohan', 'Vegeta'];
console.log(array);
let array1 = ['One Punch Man', true, 500, 'Saitama'];
console.log(array1)

// Objects
let hero = {
  heroName: 'One Punch Man',
  isActive: true,
  salary: 500,
  realName: 'Saitama',
  height: 200
}
console.log(hero);


let bandMem = ['Josh Dun', 'Tyler Joseph', 'Hayley Williams', 'Brendon Urie'];
console.log(bandMem);



let infoD = {
  firstName: 'Mel Carlo',
  lastName: 'Iguis',
  isDeveloper: true,
  hasPortfolio: true,
  age: 24
}
console.log(infoD);



// function

function displayNum(number){
  console.log(number);
}
displayNum(5000);

function displayFullName(firstName, lastName, age){
  console.log(`${firstName}`, `${lastName}` , `${age}`);
}

displayFullName('Mel Carlo', 'Iguis',24);


// return keyword

function createName(firstName, lastName){
  return `${firstName} ${lastName}`;
  console.log("I will no longer run because the function's value has been return");
}

let fullName1 = createName('Tom', 'Cruise');
let fullName2 = createName('Brad', 'Pit');
console.log(fullName1);
console.log(fullName2);

// Create a function which will be able to add two numbers.
//     -Numbers must be provided as arguments.
//     -Display the result of the addition in our console.

//   Create a function which will be able to subtract two numbers.
//     -Numbers must be provided as arguments.
//     -Display the result of subtraction in our console.

//   Create function which will be able to multiply two numbers.
//     -Numbers must be provided as arguments.
//     -Return the result of the multiplication.

//   -Create a new variable called product.
//     -This product should be able to receive the result of multiplication function.

function sumOfNum(numOne, numTwo){
  console.log(num1 + num2);
}

sumOfNum(5,6);


